﻿#include "implicitdialog.h"

ImplicitDialog::ImplicitDialog(QDialog * parent) : TvaroDialog(parent) {
	ui = new Ui::implicitdialog();
	ui->setupUi(this);
}

vtkSmartPointer<vtkActor> ImplicitDialog::GetActor()
{
	std::string expression_string1 = ui->implLine->text().toStdString();
	double bod[3];
	symbol_table_t symbol_table;
	symbol_table.add_variable("x", bod[0]);
	symbol_table.add_variable("y", bod[1]);
	symbol_table.add_variable("z", bod[2]);
	symbol_table.add_constants();
	expression_t expression1;
	expression1.register_symbol_table(symbol_table);
	parser_t parser;
	if (!parser.compile(expression_string1, expression1)) {
		std::cout << parser.error() << std::endl;
		return nullptr;
	}
	double xstart = this->ui->spinXstart->value();
	double ystart = this->ui->spinYstart->value();
	double zstart = this->ui->spinZstart->value();
	double xend = this->ui->spinXend->value();
	double yend = this->ui->spinYend->value();
	double zend = this->ui->spinZend->value();
	if (xend <= xstart || yend <= ystart || zend <= zstart)return nullptr;
	vtkSmartPointer<vtkImageData> data = vtkSmartPointer<vtkImageData>::New();
	int xmax = 512, ymax = 512/((xend - xstart)/ (yend - ystart)), zmax = 512/ ((xend - xstart) / (zend - zstart));
	
	std::cout << xmax << " " << ymax << " " << zmax << std::endl;

	data->SetExtent(0, xmax - 1, 0, ymax - 1, 0, zmax - 1);

	double dx = (xend - xstart) / xmax;
	double dy = (yend - ystart) / ymax;
	double dz = (zend - zstart) / zmax;

	data->SetSpacing(dx, dy, dz);

	data->SetOrigin(xstart, ystart, zstart);

	data->AllocateScalars(VTK_FLOAT, 1);
	float *dataPtr = (float *)data->GetScalarPointer();

	

	//vygenerujeme 3d objem podla funkcie
	int counter = 0;
	for (int i = 0; i < zmax; i++)
	{
		for (int j = 0; j < ymax; j++)
		{
			for (int k = 0; k < xmax; k++)
			{
				bod[0] = xstart+dx*k;
				bod[1] = ystart+dy*j;
				bod[2] = zstart+dz*i;
				dataPtr[counter++] = expression1.value();//pow((2 * x *x + y *y + z *z - 1), 3) - (1 / 10)*x * x* z *z*z - y *y *z *z*z;
			}
		}
	}

	//vytvorime si izoplochu
	vtkSmartPointer<vtkMarchingCubes> surface = vtkSmartPointer<vtkMarchingCubes>::New();
	surface->SetInputData(data.Get());
	surface->ComputeNormalsOn();
	
	//izoplocha hodnoty 0.000001, vtk ma nejake problemy ked to je presna nula...
	surface->SetValue(0, 0.000001);
	
	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(surface->GetOutputPort());
	mapper->Update();
	
	actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(mapper->GetInput());

	//odstranime farbu pridanu marching cubes
	polydata->GetPointData()->RemoveArray(0);

	//preskalujeme body na povodne hodnoty
	/*double bod[3];
	size_t pocet = polydata->GetNumberOfPoints();
	for (int i = 0; i < pocet; i++)
	{
		polydata->GetPoint(i, bod);
		double x, y, z;
		double half = xmax / 2 - 1;
		x = (bod[0] - half) / (xmax)* (3.0);
		y = (bod[1] - half) / (ymax)* (3.0);
		z = (bod[2] - half) / (zmax)* (3.0);
		polydata->GetPoints()->SetPoint(i, x, y, z);
	}
	polydata->Modified();*/
	return actor;
}

ImplicitDialog::~ImplicitDialog() {
	delete ui;
	
}
